const express = require('express')
const serviceRouter=express.Router();

const userRouter=require('./user.route');
const hospitalRouter=require('./hospital.route');

serviceRouter.use('/userService',userRouter);
serviceRouter.use('/hospitalService',hospitalRouter);

module.exports=serviceRouter;