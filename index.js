const express = require('express');
const route = express.Router();
const app= express();
const { Client } = require('pg')
const cors=require('cors');
app.use(express.json())
app.use(cors());
var bodyParser = require('body-parser')
const apiRoute= require ('./src/routes/index.route');
const config = require('./src/config/config');
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json())

// const server=require('http').createServer(app);

const client = new Client({
  user: 'developer',
  host: 'localhost',
  database: 'revised',
  password: 'revised',
  port: 5432,
})
client.connect(function(err) {
  if (err) throw err;
  console.log("Connected!");
});

// route.use('/',apiRoute.indexRoute);
app.get('/', function(req, res){
    console.log('app starting on port: '+port)
    res.send('tes express nodejs mongodb');
});

app.listen(config.postgres.port,()=>{
    console.log(`app is running on port ${config.postgres.port}`);
})

module.exports=route;